import React from 'react'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Navbar from './component/layout/Navbar';
import Home from './component/Pages/HomePage/Home';
import DoctorsDetails from './component/Pages/DeatailsPage/DoctorsDetails';
const App = () => {
    return (
        <Router>
            <div>
                <Navbar />
                <Switch>
                    <Route exact path="/" component={ Home }/>
                    <Route exact path="/doctorsDetails/:id" component={ DoctorsDetails }/>
                </Switch>
            </div>
        </Router>
    )
}

export default App
