import React, { useState, useContext } from "react";
import { ModalData } from "../../Contexts/ModalDataShow";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import "../Modal/style.css";
import DynamicInput from "../InputComponent/DynamicInput";
const BookingModal = () => {
  const { timeSlot, handleClose, show, handleShow } = useContext(ModalData);
  const [getBookingInfo, setBookingInfo] = useState({});
  const formDataHandler = (val) => {
    const { name, value } = val.target;
    setBookingInfo({ ...getBookingInfo, [name]: value });
  };
  const handleSubmit = () => {
    const formData = { ...getBookingInfo, timeslot: timeSlot };
    console.log("getBookingInfo", formData);
  };
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Provide Appointment Booiking Info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <DynamicInput
              label={"Time Slot"}
              type={"text"}
              placeholder={"Write..."}
              value={timeSlot}
              name={"time"}
              formDataHandler={formDataHandler}
            />
            <DynamicInput
              label={"Name"}
              type={"text"}
              placeholder={"Write..."}
              value={""}
              name={"name"}
              formDataHandler={formDataHandler}
            />
            <DynamicInput
              label={"Phone"}
              type={"text"}
              placeholder={"Write..."}
              value={""}
              name={"phone"}
              formDataHandler={formDataHandler}
            />
            <DynamicInput
              label={"Visit Reason"}
              type={"text"}
              placeholder={"Write..."}
              value={""}
              name={"visit-reason"}
              formDataHandler={formDataHandler}
            />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => handleSubmit()}>
            Book Appointment
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default BookingModal;
