import React, { useState, useEffect } from "react";
import ListOfDoctors from "./ListOfDoctors";
import { UserData } from "../../../Contexts/UserData";
import axios from "axios";
const Home = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadUser();
  }, []);
  const loadUser = async () => {
    const result = await axios.get("http://localhost:3001/users");
    console.log("result from load user", result.data);
    setData(result.data);
  };
  return (
    <div>
      <UserData.Provider
        value={{
          data
        }}
      >
        <ListOfDoctors />
      </UserData.Provider>
    </div>
  );
};
export default Home;
