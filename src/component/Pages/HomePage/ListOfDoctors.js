import React, { useState, useContext } from "react";
import { UserData } from "../../../Contexts/UserData";
import { Link } from "react-router-dom";
import userIamges from "../../../images/male-doctor.png";
import hospitalImages from "../../../images/hospital.png";
import "./style.css";
const ListOfDoctors = () => {
    const { data } = useContext(UserData);
    console.log("data", data)
  return (
    <div className="container">
      <div className="row">
        {data.map((item, index) => (
          <>
            <div className="col-sm-12 col-md-6 col-lg-6">
              <div className="card mt-5 card-box-shadow" key={index}>
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-12 col-md-6 col-lg-4">
                      <img src={userIamges} className="user-image" />
                    </div>
                    <div className="col-sm-12 col-md-6 col-lg-8">
                      <h5 className="doctor-name">{item.name}</h5>
                      <div className="hospital-name-container">
                        <img src={hospitalImages} className="hospital-img" />
                        <span className="hospital-name">{item.org}</span>
                      </div>
                      <h6 className="schedule-title my-1">Days & Timings</h6>
                      <ul className="mb-2">
                        {Object.keys(item.availibility).map((key, index) => (
                          <li className="schedule-style" key={index}>
                            {key} : {item.availibility[key]}
                          </li>
                        ))}
                      </ul>
                      <Link
                        to={`/doctorsDetails/${item.id}`}
                        className="btn appointment-btn"
                      >
                        Take Appointment
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        ))}
      </div>
    </div>
  );
};

export default ListOfDoctors;
