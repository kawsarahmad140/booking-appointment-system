import React, { useState, useEffect } from "react";
import axios from "axios";
import userIamges from "../../../images/male-doctor.png";
import hospitalImages from "../../../images/hospital.png";
import { useParams } from "react-router-dom";
import moment from "moment";
import "react-calendar/dist/Calendar.css";
import "../DeatailsPage/style.css";
import ShowCalender from "./ShowCalender";
import { ModalData } from "../../../Contexts/ModalDataShow";
const DoctorsDetails = () => {
  const [doctorInfo, setDoctorInfo] = useState([]);
  const [timeSlot, setTimeSlot] = useState("");
  const [show, setShow] = useState(false);
  const [markDate, setMarkDate] = useState([]);
  const { id } = useParams();
  useEffect(() => {
    loadUser();
  }, []);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const loadUser = async () => {
    let availableData = [];
    const result = await axios.get(`http://localhost:3001/users/${id}`);
    setDoctorInfo(result.data);
    Object.keys(result.data.availibility).map((key, index) => {
      let x = getDayHandler(key);
      availableData.push(...x);
    });
    setMarkDate(availableData);
  };
  const getDayHandler = (day) => {
    let result = [];
    var getDayName = moment().startOf("month").day(day);
    if (getDayName.date() > 7) getDayName.add(7, "d");
    var month = getDayName.month();
    while (month === getDayName.month()) {
      let temp = moment(getDayName, "DD-MM-YYYY").format("DD-MM-YYYY");
      result.push(temp);
      getDayName.add(7, "d");
    }
    return result;
  };
  return (
    <div className="details-container mt-5 pt-3">
      <ModalData.Provider
        value={{
          show,
          timeSlot,
          setTimeSlot,
          setShow,
          handleShow,
          handleClose,
        }}
      >
        <div className="row my-5 d-flex justify-content-center align-items-center ">
          <div className="col-sm-12 col-md-6 col-lg-6 ">
            <div className="card card-box-shadow ">
              <div className="card-body">
                <div className="details-image-container">
                  <img src={userIamges} className="details-image" />
                </div>
                <h5 className="text-center mt-3 doctor-name">
                  {doctorInfo.name}
                </h5>
                <div className="d-flex justify-content-center align-items-center">
                  <img src={hospitalImages} className="hospital-img" />
                  <span className="hospital-name">{doctorInfo.org}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ShowCalender data={markDate} doctorInfo={doctorInfo} />
      </ModalData.Provider>
    </div>
  );
};

export default DoctorsDetails;
