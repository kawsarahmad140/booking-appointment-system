import React, { useState, useEffect, useContext } from "react";
import Calendar from "react-calendar";
import moment from "moment";
import "react-calendar/dist/Calendar.css";
import "../DeatailsPage/style.css";
import BookingModal from "../../Modal/BookingModal";
import { ModalData } from "../../../Contexts/ModalDataShow";
import selectIconImage from "../../../images/menu.png";
const ShowCalender = (props) => {
  const selectedApointment = props.data;
  const { setTimeSlot, setShow } = useContext(ModalData);
  const [showAvailableSlots, setAvailableSlots] = useState([]);
  const [getClickedDate, setClickedDate] = useState("");
  const showSlotsHandler = (val) => {
    let getDate = val.toDateString();
    setClickedDate(getDate);
    let date = getDate.split(" ");
    let dayName = date.shift().toLowerCase();
    Object.keys(props.doctorInfo.availibility).map((key, index) => {
      if (key == dayName) {
        let slots = props.doctorInfo.availibility[key].split("-");
        let x = createTimeSlot(slots[0], slots[1]);
        setAvailableSlots(x);
      }
    });
  };
  
  const createTimeSlot = (from, to) => {
    let startTime = moment(from, "hh:mm A");
    let endTime = moment(to, "hh:mm A");
    if (endTime.isBefore(startTime)) {
      endTime.add(1, "day");
    }
    let arr = [];
    while (startTime <= endTime) {
      arr.push(new moment(startTime).format("hh:mm A"));
      startTime.add(15, "minutes");
    }
    return arr;
  };
  const showBookingDetailsModal = (time) => {
    setTimeSlot(time);
    setShow(true);
  };
  return (
    <>
      <div className="row card-box-shadow ">
        <div className="col-sm-12 col-md-6 col-lg-6">
          <div className="react-calendar">
            <Calendar
              onChange={(val) => showSlotsHandler(val)}
              tileClassName={({ date, view }) => {
                if (
                  selectedApointment.find(
                    (x) => x === moment(date).format("DD-MM-YYYY")
                  )
                ) {
                  return "react-calendar__tile--active";
                }
              }}
            />
          </div>
        </div>
        <div className="col-sm-12 col-md-6 col-lg-6">
          <h6 className="mb-2">
            Available Slots{" "}
            {getClickedDate == "" ? "" : `For ${getClickedDate}`}:
          </h6>

          {showAvailableSlots.length > 0 ? (
            showAvailableSlots.map((item, index) => (
              <span
                className="badge badge-pill badge-secondary mx-3 py-2 px-2 my-1"
                key={index}
                onClick={() => showBookingDetailsModal(item)}
              >
                {item}
              </span>
            ))
          ) : (
            <div className="image-container py-5">
              <img src={selectIconImage} style={{ maxHeight: "100px" }} />
              <h6 className="suggestion-text">
                Please click on the blue selected dates to see available slots
                for making an appointment
              </h6>
            </div>
          )}
        </div>
        <BookingModal />
      </div>
    </>
  );
};

export default ShowCalender;
