import React from 'react'
import { Link, NavLink } from 'react-router-dom';
import "../layout/style.css";
const Navbar = ()=> {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light navbar-shadow">
        <Link className="navbar-brand" to="/">
          Sqaure Health Ltd.
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">
                Back To Home
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
}

export default Navbar
