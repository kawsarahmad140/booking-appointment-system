import React, {useState} from "react";
import Form from "react-bootstrap/Form";
import moment from 'moment';

const DynamicInput = ({
  label,
  value,
  type,
  name,
  placeholder,
  formDataHandler,
}) => {
  const [inputData, setInputData] = useState(value);

  return (
    <>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>{label}</Form.Label>
        <Form.Control
          type={type}
          placeholder={placeholder}
          defaultValue={inputData}
          name={name}
          onChange={(val) => formDataHandler(val)}
        />
      </Form.Group>
    </>
  );
};

export default DynamicInput;
